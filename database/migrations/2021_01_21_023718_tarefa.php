<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Tarefa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tarefa', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_pessoa')->index();
            $table->dateTime('created_at')->index();
            $table->dateTime('deleted_at')->index()->nullable();
            $table->string('nome', 244);
            $table->enum('situacao', array('pendente','em_execucao','concluido'))->index();
            $table->text('descricao')->nullable();
            $table->foreign('id_pessoa')->references('id')->on('pessoa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tarefa');
    }
}
