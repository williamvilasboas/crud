const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').postCss('resources/css/app.css', 'public/css', [
    require('postcss-import'),
    require('tailwindcss'),
    require('autoprefixer'),
]);

/** tela administrativa **/

mix.js('resources/js/dashoard.js', 'public/js').vue();

mix.styles([
    //'resources/assets/admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
    //'resources/assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
    //'resources/assets/admin/plugins/jqvmap/jqvmap.min.css',
    'resources/css/adminlte-lite.min.css',
    //'resources/assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
    //'resources/assets/admin/plugins/summernote/summernote-bs4.css',
    'node_modules/vue-toast-notification/dist/theme-default.css',
    'resources/css/adminlte-custom.css',
], 'public/css/dashoard.css');
