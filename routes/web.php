<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard/{any?}', [\App\Http\Controllers\DashboardController::class,'inicio'])->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => ['auth','codigo_acesso']], function () {

    Route::prefix('pessoa')->group(function () {

        Route::get('/detalhe/{idPessoa?}', [\App\Http\Controllers\PessoaController::class, 'detalhe'])->name('detalhe-pessoa');

        Route::get('/listar', [\App\Http\Controllers\PessoaController::class, 'listar'])->name('listar-pessoas');

        Route::post('/', [\App\Http\Controllers\PessoaController::class, 'gravar'])->name('gravar-pessoa');

        Route::delete('/{idPessoa}', [\App\Http\Controllers\PessoaController::class, 'apagar'])->name('apagar-pessoa');

    });

    Route::prefix('tarefa')->group(function () {

        Route::get('/detalhe/{idTarefa?}', [\App\Http\Controllers\TarefaController::class, 'detalhe'])->name('tarefa');

        Route::get('/listar', [\App\Http\Controllers\TarefaController::class, 'listar'])->name('listar-tarefa');

        Route::post('/', [\App\Http\Controllers\TarefaController::class, 'gravar'])->name('gravar-tarefa');

        Route::delete('/{idTarefa}', [\App\Http\Controllers\TarefaController::class, 'apagar'])->name('apagar-tarefa');

    });
});

require __DIR__.'/auth.php';
