<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CodigoAcesso extends Model
{
    use HasFactory;
    protected $table = 'codigo_acesso';
}
