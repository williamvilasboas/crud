<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tarefa extends Model
{
    use HasFactory;
    protected $table = 'tarefa';

    public function cadastrar($pessoa)
    {
        return DB::table($this->table)->insertGetId([

        ]);
    }

    public function atualizar($idPessoa, Array $dados)
    {
        return DB::table($this->table)->where('id', $idPessoa)->update($dados);
    }

    public function detalhes(Array $params)
    {

    }

    public function apagar($idPessoa)
    {

    }
}
