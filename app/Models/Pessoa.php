<?php

namespace App\Models;

use App\Classe\PessoaClasse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Pessoa extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'pessoa';
    protected $dates = ['deleted_at'];

    public function cadastrar(PessoaClasse $pessoa)
    {
        return DB::table($this->table)->insertGetId([
            'created_at'        => date('Y-m-d H:i:s'),
            'nome'              => $pessoa->getNome(),
            'cpf'               => $pessoa->getCpf(),
            'celular'           => $pessoa->getCelular(),
            'data_nascimento'   => $pessoa->getDataNascimento()
        ]);
    }

    public function atualizar($idPessoa, Array $dados)
    {
        return DB::table($this->table)->where('id', $idPessoa)->update($dados);
    }

    public function detalhes(Array $params)
    {
        $qb = DB::table($this->table);
        $qb->whereNull("{$this->table}.deleted_at");

        if(!empty($params['select'])) {
            $qb->select($params['select']);
        }

        if(!empty($params['where'])) {
            $qb->where($params['where']);
        }

        if(!empty($params['function'])) {
            $qb->where($params['function']);
        }

        if(!empty($params['orderBy'])) {

            foreach ($params['orderBy'] as $order) {
                $qb->orderBy($order[0], $order[1]);
            }
        }

        if(!empty($params['whereNotNull'])) {

            foreach ($params['whereNotNull'] as $r) {
                $qb->whereNotNull($r);
            }
        }

        if(!empty($params['whereNull'])) {

            foreach ($params['whereNull'] as $r) {
                $qb->whereNull($r);
            }
        }

        if(!empty($params['whereDate'])) {

            foreach ($params['whereDate'] as $r) {
                $qb->whereDate($r[0], $r[1], $r[2]);
            }
        }

        if(!empty($params['groupBy'])) {

            foreach ($params['groupBy'] as $grupo) {
                $qb->groupBy($grupo);
            }
        }

        return $qb->first();
    }

    public function apagar($idPessoa)
    {
        return $this->atualizar($idPessoa, ['deleted_at' => date('Y-m-d H:i:s')]);
    }

    public function listar(Array $params)
    {
        $qb = DB::table($this->table);
        $qb->whereNull("{$this->table}.deleted_at");

        if(!empty($params['select'])) {
            $qb->select($params['select']);
        }

        if(!empty($params['where'])) {
            $qb->where($params['where']);
        }

        if(!empty($params['function'])) {
            $qb->where($params['function']);
        }

        if(!empty($params['orderBy'])) {

            foreach ($params['orderBy'] as $order) {
                $qb->orderBy($order[0], $order[1]);
            }
        }

        if(!empty($params['whereNotNull'])) {

            foreach ($params['whereNotNull'] as $r) {
                $qb->whereNotNull($r);
            }
        }

        if(!empty($params['whereNull'])) {

            foreach ($params['whereNull'] as $r) {
                $qb->whereNull($r);
            }
        }

        if(!empty($params['whereDate'])) {

            foreach ($params['whereDate'] as $r) {
                $qb->whereDate($r[0], $r[1], $r[2]);
            }
        }

        if(!empty($params['groupBy'])) {

            foreach ($params['groupBy'] as $grupo) {
                $qb->groupBy($grupo);
            }
        }

        if(!empty($params['paginate'])) {
            return $qb->paginate($params['paginate']);
        }

        if(!empty($params['limit'])) {
            $qb->limit($params['limit']);
        }

        return $qb->get();
    }
}
