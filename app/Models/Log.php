<?php

namespace App\Models;

use App\Classe\LogClasse;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Log extends Model
{
    use HasFactory;
    protected $table = 'log';

    public function cadastrar(LogClasse $log)
    {
        return DB::table($this->table)->insertGetId([
            'created_at'        => date('Y-m-d H:i:s'),
            'rota'              => $log->getRota(),
            'dados_enviados'    => $log->getDadosEnviados(),
            'retorno'           => $log->getRetorno(),
            'mensagem'          => $log->getMensagem()
        ]);
    }
}
