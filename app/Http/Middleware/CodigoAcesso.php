<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CodigoAcesso
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        $existe = \App\Models\CodigoAcesso::select('id')->where('codigo', $request->header('X-Code'))->first();
//
//        if (!$existe)
//            throw new \Exception('Acesso não autorizado.');

        return $next($request);
    }
}
