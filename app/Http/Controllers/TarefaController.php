<?php

namespace App\Http\Controllers;

use App\Service\LogService;
use App\Service\TarefaService;
use Illuminate\Http\Request;

class TarefaController extends Controller
{
    private $request;
    private $logService;
    private $tarefaService;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->logService = new LogService();
        $this->tarefaService = new TarefaService();
    }

    public function listar()
    {
        try {
            $retorno = $this->tarefaService->listar();
            return response()->json($retorno);

        } catch (\Exception $e) {
            $this->logService->gravar($e);
        }
    }
}
