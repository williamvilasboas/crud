<?php

namespace App\Http\Controllers;

use App\Service\LogService;
use App\Service\PessoaService;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PessoaController extends Controller
{
    private $request;
    private $logService;
    private $pessoaService;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->logService = new LogService();
        $this->pessoaService = new PessoaService();
    }

    public function listar()
    {
        try {
            $retorno = $this->pessoaService->listar($this->request->query());

            if ($retorno->isNotEmpty())
                foreach ($retorno as $k => $v)
                    if (!empty($v->data_nascimento))
                        $v->data_nascimento = formatarData($v->data_nascimento, 'd/m/Y');

            return response()->json($retorno);

        } catch (\Exception $e) {
            $this->logService->gravar($e);
        }
    }

    public function detalhe()
    {
        try {
            $retorno = $this->pessoaService->detalhe($this->request->route()->parameter('idPessoa'));

            if (!empty($retorno->data_nascimento))
                $retorno->data_nascimento = formatarData($retorno->data_nascimento, 'd/m/Y');

            return response()->json($retorno);

        } catch (\Exception $e) {
            $this->logService->gravar($e);
        }
    }

    public function gravar()
    {
        $this->validate($this->request, [
            'nome'              => 'required',
            'cpf'               => 'required',
            'celular'           => 'required',
            'data_nascimento'   => 'nullable'
        ]);

        try {
            $pessoa = $this->pessoaService->gravar($this->request->post());
            return response()->json(['mensagem' => __('pessoa.gravar_sucesso', ['pessoa' => $pessoa->getNome()])]);

        } catch (ValidationException $e) {
            throw $e;
        } catch (\Exception $e) {
            $this->logService->gravar($e);
        }
    }

    public function apagar()
    {
        try {
            $idPessoa = $this->request->route()->parameter('idPessoa');
            $this->pessoaService->apagar($idPessoa);
            return response()->json(['mensagem' => __('pessoa.removido')]);

        } catch (\Exception $e) {
            $this->logService->gravar($e);
        }
    }
}
