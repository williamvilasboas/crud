<?php

namespace App\Service;

use App\Classe\LogClasse;
use App\Models\Log;

class LogService
{
    private $log;

    public function __construct()
    {
        $this->log = new Log();
    }

    public function gravar(\Exception $e)
    {
        $log = new LogClasse();
        $log->setMensagem($e->getMessage());
        $log->setRota(request()->route()->getName());
        $log->setDadosEnviados(request()->all());
        $log->setRetorno($e);
        $this->log->cadastrar($log);
        abort(404, __('validation.falha'));
    }
}
