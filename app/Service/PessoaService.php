<?php

namespace App\Service;

use App\Classe\PessoaClasse;
use App\Models\Pessoa;
use Illuminate\Validation\ValidationException;

class PessoaService
{
    private $pessoa;

    public function __construct()
    {
        $this->pessoa = new Pessoa();
    }

    public function listar(Array $params)
    {
        $p = [
            'select' => [
                'pessoa.id',
                'pessoa.nome',
                'pessoa.cpf',
                'pessoa.data_nascimento',
                'pessoa.celular'
            ],
            'orderBy' => [
                ['pessoa.created_at', 'DESC']
            ],
            'paginate' => 12
        ];

        if (!empty($params['q'])) {

            $p['function'] = function($query) use ($params) {

                $cpf = retornarCpf($params['q']);
                $query->where('pessoa.nome', 'LIKE', "%{$params['q']}%");

                if (!empty($cpf))
                    $query->orWhere('pessoa.cpf', 'LIKE', '%' . retornarCpf($params['q']) . '%');
            };
        }

        return $this->pessoa->listar($p);
    }

    public function gravar(Array $dados)
    {
        $p = new PessoaClasse($dados);
        $this->dadosValidos($p);
        $p = $this->padronizarDados($p);

        if ($p->getId()) {
            $this->pessoa->atualizar($p->getId(), [
                'nome'              => $p->getNome(),
                'cpf'               => $p->getCpf(),
                'celular'           => $p->getCelular(),
                'data_nascimento'   => $p->getDataNascimento(),
            ]);
        } else {
            $id = $this->pessoa->cadastrar($p);
            $p->setId($id);
        }

        return $p;
    }

    public function detalhe($idPessoa)
    {
        $p = [
            'select' => [
                'pessoa.id',
                'pessoa.nome',
                'pessoa.cpf',
                'pessoa.celular',
                'pessoa.data_nascimento',
            ],
            'where' => [
                ['pessoa.id', '=', $idPessoa]
            ]
        ];

        return $this->pessoa->detalhes($p);
    }

    public function dadosValidos(PessoaClasse $p)
    {
        $erros = [];

        if ($p->getCpf() && !cpfValido($p->getCpf()))
            $erros['cpf'] = __('pessoa.cpf_invalido');

        if ($p->getCelular() && !telefoneValido($p->getCelular()))
            $erros['celular'] = __('pessoa.celular_invalido');

        if ($p->getDataNascimento() && !dataValida($p->getDataNascimento()))
            $erros['data_nascimento'] = __('pessoa.data_invalido');

        if (!empty($erros))
            throw ValidationException::withMessages($erros);
    }

    public function padronizarDados(PessoaClasse $p)
    {
        if ($p->getDataNascimento())
            $p->setDataNascimento(formatarData($p->getDataNascimento(), 'Y-m-d'));

        return $p;
    }

    public function apagar($idPessoa)
    {
        if (!$idPessoa)
            throw new \Exception(__('pessoa.falha_apagar'));

        return $this->pessoa->apagar($idPessoa);
    }
}
