<?php

namespace App\Classe;

class LogClasse
{
    private $id;
    private $created_at;
    private $rota;
    private $dados_enviados;
    private $retorno;
    private $mensagem;

    public function __construct(Array $dados = array())
    {
        foreach ($dados as $campo => $valor) {
            $metodo = setValorObjeto($campo);
            if (method_exists(new LogClasse, $metodo)) {
                $this->$metodo($valor);
            }
        }
    }

    public function _toArray()
    {
        $novo = [];
        foreach ($this as $k => $v) {
            $novo[$k] = $v;
        }
        return $novo;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return mixed
     */
    public function getRota()
    {
        return $this->rota;
    }

    /**
     * @param mixed $rota
     */
    public function setRota($rota): void
    {
        $this->rota = $rota;
    }

    /**
     * @return mixed
     */
    public function getDadosEnviados()
    {
        return $this->dados_enviados;
    }

    /**
     * @param mixed $dados_enviados
     */
    public function setDadosEnviados($dados_enviados): void
    {
        if (is_array($dados_enviados))
            $dados_enviados = json_encode($dados_enviados);

        $this->dados_enviados = $dados_enviados;
    }

    /**
     * @return mixed
     */
    public function getRetorno()
    {
        return $this->retorno;
    }

    /**
     * @param mixed $retorno
     */
    public function setRetorno($retorno): void
    {
        if (is_object($retorno))
            $retorno = (array)$retorno;

        if (is_array($retorno))
            $retorno = json_encode($retorno);

        $this->retorno = $retorno;
    }

    /**
     * @return mixed
     */
    public function getMensagem()
    {
        return $this->mensagem;
    }

    /**
     * @param mixed $mensagem
     */
    public function setMensagem($mensagem): void
    {
        $this->mensagem = $mensagem;
    }
}
