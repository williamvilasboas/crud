window.Vue = require('vue');
window.axios = require('axios');

import Vue from 'vue';
import VueRouter from 'vue-router';
import VueToast from 'vue-toast-notification';
import Vue2Editor from 'vue2-editor';
import VueMask from 'v-mask';
import VueSlideoutPanel from 'vue2-slideout-panel';
import VueSweetalert2 from 'vue-sweetalert2';
import VueLazyload from 'vue-lazyload';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';

import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.use(VueMask);
Vue.use(VueToast);
Vue.use(Vue2Editor);
Vue.use(VueRouter);
Vue.use(VueSlideoutPanel);
Vue.use(VueSweetalert2);
Vue.use(VueLazyload);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('carregando', require('laravel-vue-pagination'));

import App from '../vue-js/App';
import Tarefa from '../vue-js/tarefa/Listar';
import Pessoa from '../vue-js/pessoa/Listar';

const router = new VueRouter({
    model: 'history',
    base: '/tarefa',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            redirect: '/pessoa'
        },
        {
            path: '/tarefa',
            name: 'tarefa',
            component: Tarefa
        },
        {
            path: '/pessoa',
            name: 'pessoa',
            component: Pessoa
        },
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
