import axios from "axios";

export function req() {

    const instance = axios.create({
        baseURL: 'http://127.0.0.1:8000',
        timeout: 13000,
        headers: {'x-code': '123'}
    });
}
