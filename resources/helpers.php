<?php

if (!function_exists('primeiraLetraMaiscula')) {
    function primeiraLetraMaiscula($string)
    {
        $string = trim($string);
        return mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
    }
}

if (!function_exists('setValorObjeto')) {

    function setValorObjeto($campo, $nome = 'set')
    {
        $campos = explode('_', $campo);
        $nomeMetodo = $nome;

        foreach ($campos as $campo) {
            $nomeMetodo .= primeiraLetraMaiscula($campo);
        }

        return $nomeMetodo;
    }
}

if (!function_exists('cpfValido')) {

    function cpfValido($cpf = null)
    {
        if(empty($cpf)) {
            return false;
        }

        // Extrai somente os números
        $cpf = preg_replace( '/[^0-9]/is', '', $cpf );

        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }

        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        }

        // Verifica se foi informada uma sequência de digitos repetidos. Ex: 111.111.111-11
        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        // Faz o calculo para validar o CPF
        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf[$c] * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf[$c] != $d) {
                return false;
            }
        }
        return true;
    }
}

if (!function_exists('dataValida')) {

    function dataValida($data)
    {
        $array = explode('/', $data);

        //garante que o array possue tres elementos (dia, mes e ano)
        if(count($array) == 3){
            $dia = (int)$array[0];
            $mes = (int)$array[1];
            $ano = (int)$array[2];

            if(checkdate($mes, $dia, $ano)){
                return true;
            }
        }

        return false;
    }
}

if (!function_exists('telefoneValido')) {

    // A função abaixo demonstra o uso de uma expressão regular que identifica, de forma simples, telefones válidos no Brasil.
    // Nenhum DDD iniciado por 0 é aceito, e nenhum número de telefone pode iniciar com 0 ou 1.
    // Exemplos válidos: +55 (11) 98888-8888 / 9999-9999 / 21 98888-8888 / 5511988888888s
    function telefoneValido($phone)
    {
        $regex = '/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/';

        if (preg_match($regex, $phone) == false) {
            // O número não foi validado.
            return false;
        } else {
            // Telefone válido.
            return true;
        }
    }
}

if (!function_exists('formatarData')) {

    function formatarData($date, $format = 'd/m/Y', $default = '-')
    {
        if ($date) {
            $pos = strpos($date, '/');
            $existeHora = strpos($date, ':');
            $formatoData = ($pos === false) ? 'Y-m-d' : 'd/m/Y';
            $formatoData = ($existeHora === false) ? $formatoData : $formatoData . ' H:i:s';
            $data = DateTime::createFromFormat($formatoData, $date);
            return $data->format($format);
        }
        return $default;
    }
}
if (!function_exists('retornarCpf')) {

    function retornarCpf($cpf)
    {
        if (cpfValido($cpf)) {
            $cpf = retornarNumeros($cpf);
            $cpf = formatarCnpjCpf($cpf);
            return $cpf;
        }

        return false;
    }
}

if (!function_exists('retornarNumeros')) {

    function retornarNumeros($value)
    {
        if (!$value) {
            return '';
        }

        return preg_replace("/[^0-9]/", "", $value);
    }
}

if (!function_exists('formatarCnpjCpf')) {

    function formatarCnpjCpf($value)
    {
        if (!$value || $value == 0) {
            return '';
        }

        $cnpj_cpf = preg_replace("/\D/", '', $value);

        if (strlen($cnpj_cpf) < 11) {
            $cnpj_cpf = str_pad($cnpj_cpf, 11, "0", STR_PAD_LEFT);
        }

        if (strlen($cnpj_cpf) === 11) {
            return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
        }

        return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
    }
}
