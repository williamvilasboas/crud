import {TOKEN_ACESSO, TIMEOUT_BASE, BASE_URL} from '../../js/config';
import axios from 'axios';
import Vue from 'vue';

const paramsAxios = {
    timeout: TIMEOUT_BASE,
    headers: {'X-Code': TOKEN_ACESSO}
}

if (BASE_URL) {
    paramsAxios.baseURL = BASE_URL;
}

const instance = axios.create(paramsAxios);

import Carregando from "../componentes/carregando";
import Formulario from "./formulario";

export default {
    name: "ListarPessoa",
    data() {
        return {
            q: '',
            panel1Handle: '',
            pessoas: [],
            carregando: true
        }
    },
    components: {
        Carregando
    },
    mounted() {
        this.carregando = true;
        this.listar();
    },
    methods: {
        pagFormulario(idPessoa = null) {
            this.panel1Handle = this.$showPanel({
                component : Formulario,
                openOn: 'right',
                width: 500,
                props: {
                    idPessoa: idPessoa
                }
            });

            let me = this;
            this.panel1Handle.promise.then(result => {

                if (result.atualizarLista) {
                    me.listar();
                }

                if (result.mensagem) {
                    Vue.$toast.open({
                        message: result.mensagem,
                        type: 'success',
                        position: 'top-right'
                    });
                }
            });
        },
        apagar(dados) {

            this.$swal({
                html: `<p></p>Deseja apagar a pessoa <strong>${dados.nome}</strong>?`,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim',
                cancelButtonText: 'Não'
            }).then((result) => {

                if (result.value) {

                    instance.delete(`/pessoa/${dados.id}`)
                        .then(response => {

                            let v = document.querySelector(`.xcol-${dados.id}`);
                            v.remove();

                            Vue.$toast.open({
                                message: response.data.mensagem,
                                type: 'success',
                                position: 'top-right'
                            });

                        })
                        .catch(error => {

                            this.$swal(
                                'Atenção',
                                error.response.data.message,
                                'warning'
                            );

                            if (error.response.data.link) {
                                return location.href = error.response.data.link;
                            }
                        });
                }
            });
        },
        listar(page = 1) {

            instance.get(`/pessoa/listar/?page=${page}&q=${this.q}`)
                .then(response => {
                    this.carregando = false;
                    this.pessoas = response.data;
                })
                .catch(error => {
                    if (error.response.data.link) {
                        return location.href = error.response.data.link;
                    }
                });
        }
    }
}
