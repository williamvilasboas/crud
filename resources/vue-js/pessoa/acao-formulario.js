import {TOKEN_ACESSO, TIMEOUT_BASE, BASE_URL} from '../../js/config';
import axios from "axios";

const paramsAxios = {
    timeout: TIMEOUT_BASE,
    headers: {'X-Code': TOKEN_ACESSO}
}

if (BASE_URL) {
    paramsAxios.baseURL = BASE_URL;
}

const instance = axios.create(paramsAxios);

export default {
    name: "formularioPessoa",
    data() {
        return {
            name: 'formulario-pessoa',
            salvando: false,

            pessoa: {
                id: '',
                nome: '',
                cpf: '',
                celular: '',
                data_nascimento: ''
            },

            erro: {
                id: '',
                nome: '',
                cpf: '',
                celular: '',
                data_nascimento: ''
            },
        };
    },
    props: {
        idPessoa: {
            type: Number
        }
    },
    computed: {
        getIdPessoa: function () {
            return this.idPessoa;
        }
    },
    mounted() {
        if (this.getIdPessoa)
            this.getPessoa();
    },
    methods: {
        getPessoa() {

            instance.get(`/pessoa/detalhe/${this.getIdPessoa}`)
                .then(response => {
                    this.pessoa = response.data;
                })
                .catch(error => {

                    if (error.response.data.link) {
                        return location.href = error.response.data.link;
                    }
                });
        },
        fecharModal: function () {
            this.$emit('closePanel',{});
        },
        salvar() {
            let formValido = true;
            let mensagem = 'Este campo é obrigatório.';

            if (!this.pessoa.nome) {
                this.erro.nome = mensagem;
                formValido = false;
            } else  {
                this.erro.nome = '';
            }

            if (!this.pessoa.cpf) {
                this.erro.cpf = mensagem;
                formValido = false;
            } else  {
                this.erro.cpf = '';
            }

            if (!this.pessoa.celular) {
                this.erro.celular = mensagem;
                formValido = false;
            } else  {
                this.erro.celular = '';
            }

            if (!formValido) {
                return;
            }

            const formData = new FormData();
            formData.append('id', this.pessoa.id);
            formData.append('nome', this.pessoa.nome);
            formData.append('cpf', this.pessoa.cpf);
            formData.append('celular', this.pessoa.celular);
            formData.append('data_nascimento', this.pessoa.data_nascimento);

            let me = this;
            this.salvando = true;

            return new Promise((resolve, reject) => {
                axios.post(`/pessoa`, formData)
                    .then((response) => {

                        this.$emit('closePanel',{
                            atualizarLista: true,
                            mensagem: response.data.mensagem
                        });
                    })
                    .catch((error) => {
                        this.erro = error.response.data.errors;
                        reject(error.response.data.errors)
                    })
                    .finally((response) => {
                        this.salvando = false;
                    })
            })
        }
    }
}
