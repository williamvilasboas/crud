<?php

return [
    'gravar_sucesso' => ':pessoa foi gravado com sucesso.',
    'cpf_invalido' => 'Este CPF é inválido',
    'celular_invalido' => 'Este celular é inválido',
    'data_invalido' => 'Está data é inválida',
    'falha_apagar' => 'Não foi possível apagar o registro.',
    'removido' => 'Pessoa removida com sucesso.',
];
