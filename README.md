## Passo a passo

1) composer install

2) npm install

3) npm run production

4) cp .env.example .env

5) adicionar as conexões de banco dados no .env

6) php artisan key:generate

7) sudo composer dump

8) php artisan migrate

9) $ php artisan serve

## Infomações sobre a aplicação

1) Crie uma conta se caso seja o primeiro acesso

2) No painel administrativo você pode cadastrar pessoa (CRUD) com VueJs
